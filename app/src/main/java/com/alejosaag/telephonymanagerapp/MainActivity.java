package com.alejosaag.telephonymanagerapp;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        //Referencia al gestor de telefonia del Celular
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        //PhoneStateListener es Listener especial, que permite monitorear las llamadas del celular
        PhoneStateListener callStateListener = new PhoneStateListener() {

            public void onCallStateChanged(int state, String incomingNumber) {

                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    Toast.makeText(getApplicationContext(), "El telefono esta timbrando",
                            Toast.LENGTH_LONG).show();
                }
                if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    Toast.makeText(getApplicationContext(), "El telefono esta ocupado",
                            Toast.LENGTH_LONG).show();
                }

                if (state == TelephonyManager.CALL_STATE_IDLE) {
                    Toast.makeText(getApplicationContext(), "El telefono esta no ocupado/timbrando",
                            Toast.LENGTH_LONG).show();
                }
            }
        };

        telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    @OnClick(R.id.btnLlamar)
    public void makePhoneCall(View view) {
        String numero = "3196858395";
        hacerLlamada(numero);
    }

    @OnClick(R.id.btnInfo)
    public void getInfo(View view) {
        infoCel();
    }

    @OnClick(R.id.btnSms)
    public void ctaSms(View view) {
        enviarSMS("3165836059", "Hola mundo");
    }

    @OnClick(R.id.btnEmail)
    public void ctaEmail(View view) {
        enviarEmail("alejosaag.1285@gmail.com", "Prueba", "Hola mundo");
    }

    //Metodo para hacer llamadas
    public void hacerLlamada(String numero) {
        try {

            //Verificación del API, si el API es mayor a 22 se debe solicitar explicitamente el
            //permiso para hacer llamadas Manifest.permission.CALL_PHONE
            if (Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling

                    //Solicitud del permiso
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.CALL_PHONE}, 1);

                    return;
                }
            }

            //Intent para hacer llamadas
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + numero));
            startActivity(callIntent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //Metodo para obtener la información del celular
    public void infoCel() {
        try {
            //Verificación del API, si el API es mayor a 22 se debe solicitar explicitamente el
            //permiso para saber el estado del telefono en cuanto a llamadas
            // Manifest.permission.READ_PHONE_STATE

            if (Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling

                    //Solicitud del permiso
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_PHONE_STATE}, 1);

                    return;
                }
            }

            String info = "";

            //Get the instance of TelephonyManager
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                String IMEINumber = tm.getImei();
            }

            String imei = tm.getSimSerialNumber(); //Obtención del IMEI
            String codOperador = tm.getNetworkOperator();//Obtención del código del operador
            String nomOperador = tm.getNetworkOperatorName();//Obtención del nombre del operador

            int simCount = 0; //Número de SimCards del celular
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                simCount = tm.getPhoneCount();
            }

            //Obtención del tipo de telefono
            String strphoneType = "";

            int tipoTelefono = tm.getPhoneType();

            switch (tipoTelefono) {
                case (TelephonyManager.PHONE_TYPE_CDMA):
                    strphoneType = "CDMA";
                    break;
                case (TelephonyManager.PHONE_TYPE_GSM):
                    strphoneType = "GSM";
                    break;
                case (TelephonyManager.PHONE_TYPE_NONE):
                    strphoneType = "OTRO";
                    break;
            }

            info = "IMEI: " + imei;
            info += "\n";
            info += "CANTIDAD DE SIMCARD's: " + simCount;
            info += "\n";
            info += "CÓDIGO OPERADOR: " + codOperador;
            info += "\n";
            info += "NOMBRE DEL OPERADOR: " + nomOperador;
            info += "\n";
            info += "TIPO DE TELEFONO: " + strphoneType;

            ShowMessageDialog(info);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //Metodo para enviar SMS
    public void enviarSMS(String numero, String mensaje) {

        //Verificación del API, si el API es mayor a 22 se debe solicitar explicitamente el
        //permiso para enviar SMS
        // Manifest.permission.SEND_SMS

        if (Build.VERSION.SDK_INT > 22) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                    != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling

                //Solicitud del permiso
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS}, 1);

                return;
            }
        }

        //Intent para enviar SMS
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(numero, null, mensaje, pi, null);

        Toast.makeText(getApplicationContext(), "Mensaje enviado!",
                Toast.LENGTH_LONG).show();
    }

    //Metodo para enviar Email
    public void enviarEmail(String destino, String asunto, String mensaje) {

        //Intent para enviar Email
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{destino});
        email.putExtra(Intent.EXTRA_SUBJECT, asunto);
        email.putExtra(Intent.EXTRA_TEXT, mensaje);

        //requiero para hacer prompts email
        email.setType("message/rfc822");

        startActivity(Intent.createChooser(email, "Seleccione un cliente de correo:"));
    }

    //Método para imprimir mensajes en cuadros de dialogo
    void ShowMessageDialog(String mymessage) {
        new AlertDialog.Builder(this)
                .setMessage(mymessage)
                .setTitle("Mensaje")
                .setCancelable(true)

                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                            }
                        }).show();
    }
}